import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import DateField from '../src/components/common/DateField';

test('renders correctly with label', () => {
  const tree = renderer.create(<DateField label="label" value={new Date('2013-03-01T01:10:00')} />).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly without label', () => {
  const tree = renderer.create(<DateField value={new Date('2013-03-01T01:10:00')} />).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly with string-format date', () => {
  const tree = renderer.create(<DateField value="2013-03-01T01:10:00" />).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly with number-format date', () => {
  const tree = renderer.create(<DateField value={1513772524157} />).toJSON();
  expect(tree).toMatchSnapshot();
});