import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import MultilineTextField from '../src/components/common/MultilineTextField';

test('renders correctly with label', () => {
  const tree = renderer.create(<MultilineTextField label="label" text="text" />).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly without label', () => {
  const tree = renderer.create(<MultilineTextField text="text" />).toJSON();
  expect(tree).toMatchSnapshot();
});
