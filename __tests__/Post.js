import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import Post from '../src/components/Post';

test('renders correctly', () => {
  const tree = renderer.create(
    <Post
      date={new Date('2013-03-01T01:10:00')}
      authorName="authorName"
      text="text"
    />,
  ).toJSON();
  expect(tree).toMatchSnapshot();
});
