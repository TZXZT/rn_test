import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import TextField from '../src/components/common/TextField';


test('renders correctly with label', () => {
  const tree = renderer.create(<TextField label="label" value="label" />).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly without label', () => {
  const tree = renderer.create(<TextField value="label" />).toJSON();
  expect(tree).toMatchSnapshot();
});
