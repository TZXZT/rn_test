import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import PostList from '../src/components/PostList';


const posts = [
  { id: 3, created_at: '2013-03-01T01:10:00', user: { name: 'username' }, text: 'text' },
  { id: 2, created_at: '2013-03-01T01:10:00', user: { name: 'username' }, text: 'text' },
  { id: 1, created_at: '2013-03-01T01:10:00', user: { name: 'username' }, text: 'text' },
];
const anotherPost = { id: 4, created_at: '2013-03-01T01:10:00', user: { name: 'username' }, text: 'text' };

test('renders correctly with data', () => {
  const tree = renderer.create(
    <PostList posts={posts} />,
  ).toJSON();

  expect(tree).toMatchSnapshot();
});

test('renders correctly withouth data', () => {
  const tree = renderer.create(<PostList posts={[]} />)
    .toJSON();

  expect(tree).toMatchSnapshot();
});


test('rerenders itself if props.posts changed', () => {
  const wrapper = shallow(<PostList posts={posts} />, {
    state: { shouldUpdate: false },
  });
  wrapper.setProps({ posts: [anotherPost, ...posts] });
  const shouldUpdate = wrapper.state('shouldUpdate');
  expect(shouldUpdate).toBe(true);
});

test('doesnt rerenders itself if props.posts wasnt changed', () => {
  const wrapper = shallow(<PostList posts={posts} />, {
    state: { shouldUpdate: false },
  });
  wrapper.setProps({ posts });
  const shouldUpdate = wrapper.state('shouldUpdate');
  expect(shouldUpdate).toBe(false);
});