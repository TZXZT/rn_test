import axios from 'axios';
import Config from 'react-native-config';

export const loadPosts = async () => {
  const { data } = await axios.get(Config.API_URL, {
    params: { limit: Config.NUM_OF_POSTS },
  });
  return data;
};
