// @flow

import React, { Component } from 'react';
import { View, Text, NetInfo } from 'react-native';
import Config from 'react-native-config';

import { loadPosts } from '../api/postsApi';

import PostList from '../components/PostList';

type State = {
  posts: Array<Object>,
  shouldShowError: boolean,
};

const updateInterval = parseInt(Config.INTERVAL, 10);

class PostsScreen extends Component<{}, State> {
  constructor() {
    super();
    this.state = {
      posts: [],
      shouldShowError: false,
    };
  }

  async componentWillMount() {
    this.updatePosts();
  }

  async updatePosts() {
    const connInfo = await NetInfo.getConnectionInfo();
    if (connInfo.type === 'none' && !this.state.posts.length) {
      this.setState({ shouldShowError: true });
    }

    try {
      const posts = await loadPosts();
      this.setState({
        posts,
        shouldShowError: false,
      });
    } catch (err) {
      console.log(err);
    }
    setTimeout(() => this.updatePosts(), updateInterval);
  }


  renderPostList() {
    return <PostList posts={this.state.posts} />;
  }

  render() {
    return (
      <View style={styles.containerStyle}>
        {
          this.state.shouldShowError
            ? <Text style={styles.errorTextStyle}>Network error</Text>
            : this.renderPostList()
        }
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 5,
    paddingBotom: 5,
  },

  errorTextStyle: {
    fontSize: 18,
  },
};

export default PostsScreen;
