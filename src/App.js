// @flow

import React from 'react';
import { View } from 'react-native';

import PostsScreen from './screens/PostsScreen';

export default () => {
  return (
    <View style={{ flex: 1 }}>
      <PostsScreen />
    </View>
  );
};
