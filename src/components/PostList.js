// @flow

import React, { Component } from 'react';
import { ListView } from 'react-native';

import Post from './Post';

type Props = {
  posts: Array<Object>,
};

type State = {
  postsDataSource: Object,
};

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

class PostList extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      postsDataSource: ds.cloneWithRows(props.posts),
      shouldUpdate: true,
    };
  }

  componentWillReceiveProps(props: Props) {
    if (
      !this.props.posts.length ||
      this.props.posts[0].id !== props.posts[0].id
    ) {
      this.setState({
        postsDataSource: ds.cloneWithRows(props.posts),
        shouldUpdate: true,
      });
    } else {
      this.state.shouldUpdate = false;
    }
  }

  shouldComponentUpdate() {
    return this.state.shouldUpdate;
  }

  render() {
    const { containerStyle } = styles;
    return (
      <ListView
        style={containerStyle}
        dataSource={this.state.postsDataSource}
        renderRow={rowData => (
          <Post
            date={new Date(rowData.created_at)}
            authorName={rowData.user.name}
            text={rowData.text}
          />
        )}
      />
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
  },
};

export default PostList;
