// @flow

import React from 'react';
import { View } from 'react-native';

import TextFiled from './common/TextField';
import DateField from './common/DateField';
import MultilineTextField from './common/MultilineTextField';

type Props = {
  date: number,
  authorName: string,
  text: string,
};

const Post = (props: Props) => {
  const { date, authorName, text } = props;
  const { containerStyle, postHeaderStyle } = styles;
  return (
    <View style={containerStyle}>
      <View style={postHeaderStyle}>
        <TextFiled label="author: " value={authorName} />
        <DateField value={date} />
      </View>
      <MultilineTextField label="post: " text={text} />
    </View>
  );
};

const styles = {
  containerStyle: {
    borderWidth: 1,
    borderColor: '#000',
    marginBottom: 5,
    marginTop: 5,
    padding: 10,
  },
  postHeaderStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
};

export default Post;
