// @flow

import React from 'react';
import { View, Text } from 'react-native';

type Props = {
  label?: number,
  text: string,
};


const MultilineTextField = (props: Props) => {
  const { label, text } = props;
  const { textFieldStyle } = styles;
  return (
    <View>
      {label ? <Text>{label}</Text> : null}
      <Text style={textFieldStyle}>{text}</Text>
    </View>
  );
};

MultilineTextField.defaultProps = {
  label: '',
};


const styles = {
  textFieldStyle: {
    color: '#444',
  },
};

export default MultilineTextField;
