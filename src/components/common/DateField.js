// @flow

import React from 'react';
import { View, Text } from 'react-native';
import moment from 'moment';

type Props = {
  label?: number,
  value: string | number | Date,
  format?: string,
};

const DateField = (props: Props) => {
  const { label, value, format } = props;
  const { containerStyle } = styles;
  return (
    <View style={containerStyle}>
      {label ? <Text>{label}</Text> : null}
      <Text>{moment(value).format(format)}</Text>
    </View>
  );
};

DateField.defaultProps = {
  label: '',
  format: 'DD/MM/YYYY hh:mm',
};

const styles = {
  containerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
};

export default DateField;
