// @flow

import React from 'react';
import { View, Text } from 'react-native';

type Props = {
  label?: number,
  value: string,
};

const TextField = (props: Props) => {
  const { label, value } = props;
  const { containerStyle, valueFieldStyle } = styles;
  return (
    <View style={containerStyle}>
      {label ? <Text>{label}</Text> : null}
      <Text style={valueFieldStyle}>{value}</Text>
    </View>
  );
};

TextField.defaultProps = {
  label: '',
};


const styles = {
  containerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  valueFieldStyle: {
    color: '#444',
  },
};

export default TextField;
