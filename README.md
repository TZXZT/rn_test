# rn_test

## Description
loading and displaying set of posts on startup. checking for updates every 10 seconds (by default, can be changed in .env file)

if there is no internet connection on startup, error message will be displayed.

But if the connection was lost while application was running, the error will not be displayed, instead all of last loaded posts will remain on the screen.

As soon as the connection is restored, the application will return to normal mode

##
### Instalaton
`npm install` or `yarn` inside root directory

to run test - `npm run test` or `yarn test`

### Run this app in android emulator
- start android emulator
- type `react-native run-android`

### Run app on android device in dev mode
- connect device to computer via usb
- type `react-native run-android`
